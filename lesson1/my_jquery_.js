function my$(elementId) {
	// Находим нужный нам элемент в документе
	var el = document.getElementById(elementId);
	// Создаем объект, одним из свойств которого является оригинальный элемент
	var obj = {
		original: el,
		
		// добавляем метод, который показывает элемент
		myshow() {
			this.original.style.display = 'block';
		},
		
		// добавляем метод, который скрывает элемент
		myhide() {
			this.original.style.display = 'none';
		},

		// добавляем метод
        mytoggle() {
			// Проверяем , что метод  mytoggle вообще вызывается
            //console.log('mytoggle');
			var display = this.original.style.display;
            //console.log('display: ' + display);

            if (display === "none" ) {
                //console.log('hidden'); // проверяем результат сравнения
                //this.myshow();
                this.original.style.display = 'block';
            }
            else {
               // console.log('visible'); // проверяем результат сравнения
                //this.myhide();
                this.original.style.display = 'none';
			};
		}



	}
	// Возвращаем наш объект
	return obj;
}

