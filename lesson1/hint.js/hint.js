function my$(elementId) {
	// Находим нужный нам элемент в документе
	var el = document.getElementById(elementId);
	// Создаем объект, одним из свойств которого является оригинальный элемент
	var obj = {
		original: el,
		
		// добавляем метод, который показывает элемент
		myshow() {
			this.original.style.display = 'block';
		},
		
		// добавляем метод, который скрывает элемент
		myhide() {
			this.original.style.display = 'none';
		},  
		
		mytoggle() {
			if(this.original.style.display == 'none') { // если элемент скрыт
				this.myshow(); // то показываем его
			} else { // иначе
				this.myhide(); // скрываем
			}
		}
	}
	// Возвращаем наш объект
	return obj;
}